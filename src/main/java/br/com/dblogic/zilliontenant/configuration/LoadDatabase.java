package br.com.dblogic.zilliontenant.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.dblogic.zilliontenant.model.User;
import br.com.dblogic.zilliontenant.repository.UserRepository;

@Configuration
public class LoadDatabase {
    
    final static Logger logger = LoggerFactory.getLogger(LoadDatabase.class);
    
    @Autowired
    public UserRepository userRepository;

	@Bean
	CommandLineRunner initDatabase() {
		return args -> {
			logger.info("Preloading: " + userRepository.save(new User("Bilbo Baggins", "burglar")));
			logger.info("Preloading: " + userRepository.save(new User("Frodo Baggins", "thief")));
			logger.info("Preloading: " + userRepository.save(new User("Samwise Gamgee", "thief")));
			logger.info("Preloading: " + userRepository.save(new User("Gandalf the Grey", "mage")));
			logger.info("Preloading: " + userRepository.save(new User("Aragorn II", "king")));
			logger.info("Preloading: " + userRepository.save(new User("Saruman the White", "mage")));
		};
	}
}