package br.com.dblogic.zilliontenant.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import br.com.dblogic.zilliontenant.security.CustomAccessDeniedHandler;
import br.com.dblogic.zilliontenant.security.JwtAuthenticationFilter;
import br.com.dblogic.zilliontenant.security.JwtLoginFilter;
import br.com.dblogic.zilliontenant.security.JwtAuthenticationEntryPoint;

@Configuration
@EnableWebSecurity
public class JwtSecurityConfiguration extends WebSecurityConfigurerAdapter {
		
	@Autowired
	private JwtAuthenticationEntryPoint entryPoint;
	
	@Autowired
	private CustomAccessDeniedHandler accessDeniedHandler;
					
	@Override
	protected void configure(HttpSecurity http) throws Exception { 
        http
        	.csrf().disable()
        	.authorizeRequests()
        	.antMatchers("/login").permitAll()
        	.antMatchers("/hello/**").permitAll()
        	.antMatchers("/user/**").authenticated()
        	.antMatchers("/user/findAll").hasRole("ADMIN")
        .and()
        	.exceptionHandling()
        	.accessDeniedHandler(accessDeniedHandler)
        	.authenticationEntryPoint(entryPoint)
        .and()
        	.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        
        	// filters
        	http.addFilterBefore(new JwtLoginFilter("/login", authenticationManager()), UsernamePasswordAuthenticationFilter.class)
        		.addFilterBefore(new JwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
        
        	http.headers().cacheControl();
	}
	
	@Override
	protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
	    auth.inMemoryAuthentication()
	        	.withUser("admin")
	        	.password(encoder()
	        	.encode("adminPass"))
	        	.roles("ADMIN")
	        .and()
	        	.withUser("user")
	        	.password(encoder()
	        	.encode("userPass"))
	        	.roles("USER");
	}
	 
	@Bean
	public PasswordEncoder encoder() {
	    return new BCryptPasswordEncoder();
	}

}