package br.com.dblogic.zilliontenant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.dblogic.zilliontenant.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
        
}