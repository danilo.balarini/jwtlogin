package br.com.dblogic.zilliontenant.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.dblogic.zilliontenant.model.User;
import br.com.dblogic.zilliontenant.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	public UserService userService;

	@GetMapping("/findAll")
	public List<User> findAll() {
		return userService.findAll();
	}

	@GetMapping("/findById/{id}")
	public Optional<User> findById(@PathVariable Long id) {
		return userService.findById(id);
	}

	@PostMapping("/save")
	public User save(@RequestBody User user) {
		return userService.save(user);
	}

	@GetMapping("/delete/{id}")
	public void deleteById(@PathVariable Long id) {
		userService.deleteById(id);
	}

	@PostMapping("/delete")
	public void delete(@RequestBody User user) {
		userService.delete(user);
	}

}