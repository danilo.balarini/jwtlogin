package br.com.dblogic.zilliontenant.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dashboard")
public class DashBoardController {

	@GetMapping({ "/", "" })
	public String index() {
		return "Dashboard if exists one";
	}

}
