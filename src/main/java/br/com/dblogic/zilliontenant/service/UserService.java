package br.com.dblogic.zilliontenant.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dblogic.zilliontenant.model.User;
import br.com.dblogic.zilliontenant.repository.UserRepository;

@Service
public class UserService {
    
    @Autowired 
    public UserRepository userRepository;
    
    public List<User> findAll() {
        return userRepository.findAll();
    }
    
    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }
    
    public User save(User user) {
        return userRepository.save(user);
    }

    public User update(User user) {
        return userRepository.save(user);
    }
    
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }
    
    public void delete(User user) {
        userRepository.delete(user);
    }
    
}